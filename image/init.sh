#!/bin/bash

set -ex

function replace2 {
    VAR="$1"
    #Escape forward slashes in any replacement value
    VALUE=$(echo $2 | sed 's/\//\\\//g')
    FILE="$3"

    echo "  Replacing [$VAR] with [$VALUE]"

    sed -i "s/$VAR/$VALUE/g" "$FILE"
}

function replace {
    VAR="{{$1}}"
    replace2 $VAR $2 $3
}

_SLEEP=1
_DATABASE_DRIVER=${DATABASE_DRIVER:-"com.mysql.jdbc.Driver"}
_DATABASE_USER=${DATABASE_USER:-"vcruser"}
_DATABASE_PASSWORD=${DATABASE_PASSWORD:-"vcrpassword"}
_DATABASE_NAME=${DATABASE_NAME:-"vcr"}
_DB_HOST=${DB_HOST:-"database"}
_DB_PORT=${DB_PORT:-"3306"}

_DATABASE_JDBC="jdbc:mysql://${_DB_HOST}:${_DB_PORT}/${_DATABASE_NAME}"

_ADMIN_CONF_DIR=${ADMIN_CONF_DIR:-"/conf"}
_VCR_BASE_URI=${VCR_BASE_URI:-"http://localhost:8080/VirtualCollectionRegistry"}
_PID_PROFILE=${PID_PROFILE:-vcr.pid.dummy}
_PID_SERVICE_BASE_URI=${PID_SERVICE_BASE_URI:-"http://pid-vm04.gwdg.de:8080/handles/"}
_PID_SERVICE_PREFIX=${PID_SERVICE_PREFIX:-"11148"}
_PID_SERVICE_USERNAME=${PID_SERVICE_USERNAME:-"pidserviceusername"}
_PID_SERVICE_PASSWORD=${PID_SERVICE_PASSWORD:-"pidserviceupassword"}
_PID_SERVICE_INFIX=${PID_SERVICE_INFIX:-"VC-"}
_TOMCAT_USER_NAME=${TOMCAT_USER_NAME:-"test"}
_TOMCAT_USER_PASSWORD=${TOMCAT_USER_PASSWORD:-"test"}
_TOMCAT_ADMIN_NAME=${TOMCAT_ADMIN_NAME:-"admin"}
_TOMCAT_ADMIN_PASSWORD=${TOMCAT_ADMIN_PASSWORD:-"admin"}

_AUTH_SHIBBOLETH=${AUTH_SHIBBOLETH:-"0"}
_HOSTNAME=${VCR_HOSTNAME:-"beta-vcr.clarin.eu"}
_LOG_LEVEL=${LOG_LEVEL:-"INFO"}

_SWITCHBOARD_URL=${SWITCHBOARD_URL:-https://switchboard.clarin.eu/#/xyz}
_ENABLE_SWITCHBOARD_FOR_RESOURCES=${ENABLE_SWITCHBOARD_FOR_RESOURCES:-true}
_ENABLE_SWITCHBOARD_FOR_COLLECTIONS=${ENABLE_SWITCHBOARD_FOR_COLLECTIONS:-false}
_VCR_MODE=${VCR_MODE:-alpha}

#Template web application context.xml
CONTEXT_XML_FILE=/srv/tomcat8/webapps/ROOT/META-INF/context.xml
echo "Updating [${CONTEXT_XML_FILE}]:"
replace "DB_DRIVER" ${_DATABASE_DRIVER} ${CONTEXT_XML_FILE}
replace "DB_USERNAME" ${_DATABASE_USER} ${CONTEXT_XML_FILE}
replace "DB_PASSWORD" ${_DATABASE_PASSWORD} ${CONTEXT_XML_FILE}
replace "JDBC_URL" ${_DATABASE_JDBC} ${CONTEXT_XML_FILE}
replace "ADMIN_CONF_DIR" ${_ADMIN_CONF_DIR} ${CONTEXT_XML_FILE}
replace "VCR_BASE_URI" ${_VCR_BASE_URI} ${CONTEXT_XML_FILE}

replace "SWITCHBOARD_URL" ${_SWITCHBOARD_URL} ${CONTEXT_XML_FILE}
replace "ENABLE_SWITCHBOARD_FOR_RESOURCES" ${_ENABLE_SWITCHBOARD_FOR_RESOURCES} ${CONTEXT_XML_FILE}
replace "ENABLE_SWITCHBOARD_FOR_COLLECTIONS" ${_ENABLE_SWITCHBOARD_FOR_COLLECTIONS} ${CONTEXT_XML_FILE}
replace "VCR_MODE" ${_VCR_MODE} ${CONTEXT_XML_FILE}

#Template logging configuration
LOG4J_FILE="/srv/tomcat8/webapps/ROOT/WEB-INF/classes/log4j.properties"
echo "Updating [${LOG4J_FILE}]:"
replace2 "log4j.rootLogger=DEBUG,Console" "log4j.rootLogger=${_LOG_LEVEL},File" ${LOG4J_FILE}
replace2 "log4j.logger.eu.clarin.cmdi.virtualcollectionregistry=INFO" "log4j.logger.eu.clarin.cmdi.virtualcollectionregistry=${_LOG_LEVEL}" ${LOG4J_FILE}
echo "log4j.logger.wicket-webjars=WARN"

#Template authentication configuration
TOMCAT_USER_TEMPLATE_FILE="/srv/tomcat8/conf/tomcat-users.xml.template"
TOMCAT_USER_FILE="/srv/tomcat8/conf/tomcat-users.xml"
ADMIN_CONF_FILE="${_ADMIN_CONF_DIR}/vcr-admin.conf"

#Auth templating is run on every container start
echo "Templating authentication"
if [ "${_AUTH_SHIBBOLETH}" -eq "1" ]; then
    echo "Enabling shibboleth authentication"
    cp /srv/tomcat8/webapps/ROOT/WEB-INF/web-shib.xml /srv/tomcat8/webapps/ROOT/WEB-INF/web.xml
    replace "HOSTNAME" "${_HOSTNAME}" /srv/tomcat8/webapps/ROOT/WEB-INF/shhaa.xml
else
    echo "Enabling local authentication"
    cp /srv/tomcat8/webapps/ROOT/WEB-INF/web-basic-local.xml /srv/tomcat8/webapps/ROOT/WEB-INF/web.xml
    if [ -f "${TOMCAT_USER_TEMPLATE_FILE}" ]; then
        echo "Updating [${TOMCAT_USER_FILE}]:"
        #Separate cp and rm instead of mv to avoid overlay fs issues
        #See: https://github.com/moby/moby/issues/25409
        cp ${TOMCAT_USER_TEMPLATE_FILE} ${TOMCAT_USER_FILE}
        chown tomcat:tomcat ${TOMCAT_USER_FILE}
        replace "USER_NAME" ${_TOMCAT_USER_NAME} ${TOMCAT_USER_FILE}
        replace "USER_PASS" ${_TOMCAT_USER_PASSWORD} ${TOMCAT_USER_FILE}
        replace "ADMIN_NAME" ${_TOMCAT_ADMIN_NAME} ${TOMCAT_USER_FILE}
        replace "ADMIN_PASS" ${_TOMCAT_ADMIN_PASSWORD} ${TOMCAT_USER_FILE}
    fi
fi

#Template spring profile to properly configure pid minting
#Allowed values: vcr.pid.dummy, vcr.pid.epic, vcr.pid.gwdw
#replace2 "vcr.pid.dummy" "${_PID_PROFILE}" /srv/tomcat8/webapps/ROOT/WEB-INF/web.xml

#Template wicket mode

if [ "${_VCR_MODE}" == "prod" ]; then
    echo "Enabling wicket deployment mode"
    echo "JAVA_OPTS=\"-Dwicket.configuration=deployment -Dfile.encoding=UTF8\"" > /srv/tomcat8/bin/setenv.sh
else
    echo "Enabling wicket development mode"
    echo "JAVA_OPTS=\"-Dwicket.configuration=development -Dfile.encoding=UTF8\"" > /srv/tomcat8/bin/setenv.sh
fi

#Wait for database to become available
while ! echo exit | nc -z ${_DB_HOST} 3306 </dev/null; do echo 'Waiting for database to become available'; sleep "${_SLEEP}"; done